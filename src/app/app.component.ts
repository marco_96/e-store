import { Component } from '@angular/core';
import { AuthService } from './servicios/auth.service';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private authService: AuthService,
    private router: Router
  ) {
    this.initializedApp();
  }

  initializedApp(){
    this.platform.ready().then(() =>{
      this.authService.authState.subscribe(state => {
        console.log(state);
        if (state) {
          this.router.navigate(['members', 'tabs']);
        } else {
          this.router.navigate(['login']);
        }
      })
    })
  }

  logout(){
    this.authService.logout();
  }
}
