import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import { ApiService } from '../servicios/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: any = [];
  constructor(private authService: AuthService, public _apiService: ApiService) { }

  ngOnInit() {
  }

  login(){
    this._apiService.vUser().subscribe((res:any) => {
      // console.log("SUCCESS ===",res);
      this.authService.login();
    },(error: any)=>{
      // console.log("ERROR ===",error);
    })
  }

}
