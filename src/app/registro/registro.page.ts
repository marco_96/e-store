import { Component, OnInit } from '@angular/core';
import { ApiService } from '../servicios/api.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  nombre: any;
  email: any;
  password: any;
  type: any;
  constructor(public _apiService: ApiService) { }

  ngOnInit() {
  }

  addUser(){
    let data = {
      nombre: this.nombre,
      email: this.email,
      password: this.password,
      type: this.type,
    }

    this._apiService.addUser(data).subscribe((res:any) => {
      this.nombre="";
      this.email="";
      this.password="";
      this.type="";
    },(error:any)=> {
    })
  }

}
