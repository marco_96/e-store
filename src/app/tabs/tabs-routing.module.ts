import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuardService } from '../servicios/auth-guard.service';

const routes: Routes = [
  {
    path: 'tabs',
    canActivate: [AuthGuardService],
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            canActivate: [AuthGuardService],
            loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children:[
          {
            path:'',
            canActivate: [AuthGuardService],
            loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
        
      },
      {
        path: 'tab3',
        children:[
          {
            path:'',
            canActivate: [AuthGuardService],
            loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
        
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    canActivate: [AuthGuardService],
    redirectTo: '/members/tabs/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
