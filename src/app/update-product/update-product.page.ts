import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../servicios/api.service';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.page.html',
  styleUrls: ['./update-product.page.scss'],
})
export class UpdateProductPage implements OnInit {
  id: any;
  nombre: any;
  precio: any;
  cantidad: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _apiService : ApiService
  ) {
    this.route.params.subscribe((param:any) => {
      this.id = param.id;
      console.log(this.id);
      this.getProduct(this.id);
    })
   }

  ngOnInit() {
  }

  getProduct(id){
    this._apiService.getProduct(id).subscribe((res:any) => {
      console.log("SUCCESS ===",res);
      let p = res[0];
      this.nombre = p.nombre;
      this.precio = p.precio;
      this.cantidad = p.cantidad;
    },(error: any)=>{
      console.log("ERROR ===",error);
    })
  }

  updateProduct(){
    let data = {
      nombre: this.nombre,
      precio: this.precio,
      cantidad: this.cantidad,
    }

    this._apiService.updateProduct(this.id,data).subscribe((res:any) => {
      console.log("SUCCESS", res);
      this.router.navigateByUrl('/members/tabs');
    }, (err:any) => {
      console.log("ERROR",err);
    })
  }

}
