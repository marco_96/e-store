import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Product {
  id: number;
  nombre: string;
  precio: number;
  cantidad: number;
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  data: Product[] = [
    {id:1,nombre:'Funko Pop Moana',precio:522,cantidad:10},
    {id:2,nombre:'Funko Pop Stich',precio:522,cantidad:10},
    {id:3,nombre:'Funko Pop Marcus',precio:522,cantidad:10},
    {id:4,nombre:'Funko Pop Ang',precio:522,cantidad:10},
    {id:5,nombre:'Funko Pop Ryuk',precio:522,cantidad:10},
  ]

  private cart=[];
  private countI= new BehaviorSubject(0);

  constructor() { }

  getProducts(){
    return this.data;
  }
}
