import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState = new BehaviorSubject(false);

  constructor(
    private storage: Storage, 
    private plt: Platform) { 
      this.plt.ready().then(() => {//verify if the platform is ready
        this.checkToken();
      })
    }

    login() {
      return this.storage.set(TOKEN_KEY, 'Something 12345').then(res => {
        this.authState.next(true);
      });
    }

    logout() {
      return this.storage.remove(TOKEN_KEY).then(() => {
        this.authState.next(false);
      });
    }

    isAuthenticaded(){
      return this.authState.value;
    }

    checkToken(){
      return this.storage.get(TOKEN_KEY).then(res => {
        if(res){
          this.authState.next(true);
        }
      });
    }
}
