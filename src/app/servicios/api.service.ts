import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ApiService{
    headers: HttpHeaders;

    constructor(public http: HttpClient) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', '*');
    }

    addProduct(data){
        return this.http.post('http://localhost/Ionic/e-store/crud/create.php',data);
    }

    getProducts(){
        return this.http.get('http://localhost/Ionic/e-store/crud/get.php',{responseType: 'json'});
    }

    deleteProduct(id){
        return this.http.delete('http://localhost/Ionic/e-store/crud/delete.php?id='+id);
    }

    getProduct(id){
        return this.http.get('http://localhost/Ionic/e-store/crud/getSingle.php?id='+id);
    }

    updateProduct(id, data){
        return this.http.put('http://localhost/Ionic/e-store/crud/update.php?id='+id,data);
    }

    //user
    addUser(data){
        return this.http.post('http://localhost/Ionic/e-store/crud/createuser.php',data);
    }

    vUser(){
        return this.http.get('http://localhost/Ionic/e-store/crud/verify_user.php');
    }

}