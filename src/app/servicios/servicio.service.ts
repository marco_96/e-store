import {Injectable} from '@angular/core'    ;
import { HttpClient} from '@angular/common/http';
import { ApiService } from '../servicios/api.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ServicioService{

    private carrito=[];
    products: any = [];
    nombre:any;
    precio:any;
    cantidad:any;
    private carritoNumeroItems = new BehaviorSubject(0);

    constructor(private http:HttpClient, public _apiService: ApiService){

    }

    getCart(){
        return this.carrito;
    }

    getCartNumberI(){
        return this.carritoNumeroItems;
    }

    dismissQProduct(product){
        for (let [index, p] of this.carrito.entries()){
            if (p.id === product.id){
                p.cantidad -= 1; if (p.cantidad == 0){
                    this.carrito.splice(index, 1);
                }
            }
        }
        this.carritoNumeroItems.next(this.carritoNumeroItems.value - 1);
    }

    dismissProduct(product){
        for (let [index, p] of this.carrito.entries()){
            if (p.id === product.id){
                this.carritoNumeroItems.next(this.carritoNumeroItems.value - p.cantidad);
                this.carrito.splice(index, 1);
            }
        }
    }

    getProducts(){
        this._apiService.getProducts().subscribe((res:any) => {
          this.products =  res;
        },(error: any)=>{
        })
    }

    addtoCart(product){
        let added = false;
        for (let p of this.carrito){
            if(p.id === product.id){
                p.cantidad += 1;
                added = true;
                break;
            }
        }
        if(!added){
            this.carrito.push(product);
        }

        this.carritoNumeroItems.next(this.carritoNumeroItems.value + 1);
    }

}    
    
    
