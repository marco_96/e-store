import { Component } from '@angular/core';
import { ApiService } from '../servicios/api.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  nombre: any;
  precio: any;
  cantidad: any;
  products: any = [];
  constructor(public _apiService: ApiService) {
    this.getProducts();
  }
    
  addProduct(){
    let data = {
      nombre: this.nombre,
      precio: this.precio,
      cantidad: this.cantidad,
    }

    this._apiService.addProduct(data).subscribe((res:any) => {
      this.nombre="";
      this.precio="";
      this.cantidad="";
      this.getProducts();
    },(error:any)=> {
    })
  }

  getProducts(){
    this._apiService.getProducts().subscribe((res:any) => {
      console.log("SUCCESS ===",res);
      this.products =  res;
    },(error: any)=>{
      console.log("ERROR ===",error);
    })
  }

  deleteProduct(id){
    this._apiService.deleteProduct(id).subscribe((res:any) => {
      console.log("SUCCESS");
      this.getProducts();
    },(err: any)=>{
      console.log("ERROR ===", err);
    })
  }

  doRefresh(e){
    this.getProducts();
    setTimeout(() => {
      console.log('Async operation has ended');
      e.target.complete();
    }, 1000);
  }

}
