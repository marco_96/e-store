import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { ServicioService } from '../servicios/servicio.service';
import { ApiService } from '../servicios/api.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  carrito: any = [];

  constructor(
    private carritoServicio: ServicioService,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public _apiService: ApiService
  ) { }

  ngOnInit() {
    this.carrito= this.carritoServicio.getCart();
  }

  dismissQProduct(product){
    this.carritoServicio.dismissQProduct(product);
  }

  increaseQProduct(product){
    this.carritoServicio.addtoCart(product);
  }

  dismissProduct(product){
    this.carritoServicio.dismissProduct(product);
  }

  getTotal(){
    return this.carrito.reduce((i, j) => i + j.precio * j.cantidad, 0);
  }

  close(){
    this.modalCtrl.dismiss();
  }

  async checkout(){

    let alert = await this.alertCtrl.create({
      header: "Gracias por su elección",
      message: "Su pedido esta siendo procesado",
      buttons: ['OK']
    });
    alert.present().then(() => {

    });
  }

}
