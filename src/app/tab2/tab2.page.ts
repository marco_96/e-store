import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from '../servicios/api.service';
import { ServicioService } from '../servicios/servicio.service';
import { AlertController, ModalController } from '@ionic/angular';
import { CartPage } from '../cart/cart.page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  nombre: any;
  precio: any;
  cantidad: any;
  products: any = [];
  carrito=[];

  countItems: BehaviorSubject<number>;

  constructor(public _apiService: ApiService, public servicio: ServicioService,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,) {
    this.getProducts();
  }

  ngOnInit(){
  }

  getProducts(){
    this._apiService.getProducts().subscribe((res:any) => {
      console.log("SUCCESS ===",res);
      this.products =  res;
    },(error: any)=>{
      console.log("ERROR ===",error);
    })
  }

  async openCart() {
    const modal = await this.modalCtrl.create({
      component: CartPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  addtoCart(product){
    product.cantidad=1;
    console.log("agregado satisfactoriamente");
    this.servicio.addtoCart(product);
  }

}
