<?php
include "config.php";
$input =file_get_contents('php://input');
$message = array();

// $id= $_GET['id'];
$id = ($_GET['id'] !== null && (int)$_GET['id'] > 0)? mysqli_real_escape_string($con, (int)$_GET['id']) : false;

if(!$id)
{
  return http_response_code(400);
}
$q = mysqli_query($con, "DELETE FROM `product` WHERE `id` = '{$id}' LIMIT 1");

if($q){
    http_response_code(201);
    $message['status'] = "success";
}else{
    http_response_code(422);
    $message['status'] = "Error";
}

//only in development
echo json_encode($message);
echo mysqli_error($con);
